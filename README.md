# DPD Music

Each song goes in its own subdirectory, which should be the name of the song in lowercase, hyphenated.

Source files can included sheet music, chord sheets, or tablature (`pdf`), and usually an initial scratch track with which people can record their instruments against. Scratch tracks can go in `src/scratch`. The scratch track filename should be `<song-title>-scratch` with whatever extention (e.g., `hello-world-scratch.wav`). And feel free to put your **Digital Audio Workstation (DAW)** session files and any other files used to generate the scratch track in `src`.

Don't commit individual takes. Consider the file size of what is being committed and try not to bloat the repository (but have as many uncommitted files locally as you like for your own instrument takes).

Finished takes of each instrument go in `dist`. Probably use `wav` files for exported finished takes, unless a different file format makes more sense and opens in any DAW. Ideally you should have 1 finished take per instrument. So even if you have many inidividual takes which are spliced/layered together in your DAW, your finished export should be a single file. Unless it really makes sense to have multiple finished takes (like if you were recording 2 different instruments). If your DAW exports peak files along with the finished `wav`, feel free to commit those in `dist` as well.

Finished take file names should be lowercase, hypenated, with this format: `<song-title>-<username>-<instrument>` (e.g., `hello-world-pshryock-acoustic-guitar.wav`).

## Getting started

1. Open your DAW of choice (e.g., Pro Tools) and start a new multitrack session.
2. Import the scratch track.
3. Enable a metronome, and set it to the desired tempo in beats per minute (bpm).
4. Import any other finished takes from `dist` (each on their own channel). If everyone uses the same metronome tempo and scratch track, then all exported takes should line up perfectly without needing to adjust anyone's timing.
5. Add a new channel for your own instrument and configure input/output settings as needed.
6. Feel free to adjust any of the tracks volume and panning as needed, so you hear what you need to hear while recording.
7. Record your instrument and save your session.
8. Export your finished take in `dist`.
9. Commit your changes and push up to Bitbucket.

## Updating your own take(s)

If you've already exported your finished take to `dist`, committed it, and shared it with the group, you can still modify your take later. Just re-export your take in `dist` using the same filename as before, and commit the change. At the end, when someone mixes all the takes together, they'll use the latest and greatest versions from `dist`.

## Credits

Add your name and instrument(s) to `CONTRIBUTORS.md` in each song directory that you contribute to.
